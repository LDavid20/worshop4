<?php
  include('functions.php');
  // debuger:
  if(isset($_POST['full_name']) && isset($_POST['email']) && isset($_POST['idcarrera'])) {
    $saved = saveStudent($_POST);
    if($saved) {
      header('Location: /practica/crud/?status=success');
    } else {
      header('Location: /practica/crud/?status=error');
    }
  } else {
    header('Location: /practica/crud/?status=error');
  }
