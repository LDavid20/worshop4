<?php
include('functions.php');

$id = $_GET['id'];
if($id) {
  $student = getStudent($id);
  if($student) {
    $deleted = deleteStudent($id);
    if($deleted) {
      header('Location: /practica/crud/?status=success');
    } else {
      header('Location: /practica/crud/?status=error');
    }
  } else {
    header('Location: /practica/crud/?status=error');
  }
} else {
  header('Location: /practica/crud/index.php');
}